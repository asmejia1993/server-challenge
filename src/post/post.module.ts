import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { PostSchema } from './schema/post.schema';

@Module({
    imports: [MongooseModule.forFeature(
        [
            { name: 'Post', schema: PostSchema }
        ]), HttpModule],
    controllers: [PostController],
    providers: [PostService]
})
export class PostModule {}
