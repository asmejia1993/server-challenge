import { Document } from 'mongoose';

export interface Post extends Document {
    id?: string;
    title: string;
    author: string;
    created_at: string;
    time:string;
    url: string;
    hide: boolean;
    story_id: string;
}