import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreatePostDto } from './dto/create-post.dto';
import { Post } from './interface/Post';

@Injectable()
export class PostService {

    initPosts: CreatePostDto[]  = [];
        
    constructor(@InjectModel('Post') private readonly postModel: Model<Post>) {
    }

    async getPosts() {
        return await this.postModel.find().sort({created_at: 'desc'});
    }
    

    async removePost(id: string) {
        try {
            await this.postModel.findByIdAndRemove(id);
        } catch (error) {
            console.error(error);
            
        }
    }

    
}
