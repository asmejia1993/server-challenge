import * as mongoose from 'mongoose';
import * as request from 'supertest';
import axios from 'axios';
import { Delete } from '@nestjs/common';

const url = 'http://localhost:3000/v1/';

beforeAll(async() => {
    await mongoose.connect('mongodb://localhost/test-data');
    await mongoose.connection.db.dropDatabase();
});

afterAll(async () => {
    await mongoose.disconnect();
});

describe('POST', () => {
    it('should get all post', () => {
        return request(url)
        .get('/post')
        .set('Accept', 'application/json')
        .expect(200);
    });

    it('should remove a post by id', async () => {
        const id = ''
        await axios.delete(`${url}/post/${id}`);

    });
});